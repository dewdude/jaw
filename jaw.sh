#!/bin/bash
#jaw v.73 - jukebox for asterisk wizard
#Jay Moore/NQ4T - https://gitlab.com/dewdude/jaw
#Licensed under GPL-3.0-or-later

# Works like this:
#  Put all your albums in your Asterisk sounds directory: /jukebox root/album/track#.ulaw
#  Run script, enter prompts.
#  Copy .conf, include in extensions.conf, assign extension.

# Modify script below if your default sounds isn't /var/lib/asterisk/sounds/en

setup() {
printf "[$name-main]\nexten = s,1,Answer()\nsame =n,Set(VOLUME(TX)=$vol)\nsame = n(return),Set(m=1)\nsame = n(loop),Background($rootfolder/$introfile)\n"
printf "same = n,WaitExten($delay)\nsame = n,Set(m=\$[\${m} + 1])\nsame = n,GoToIf(\$[\"\${m}\" < \"$loop\"]?loop)\nsame = n,Background($rootfolder/$goodbye)\nsame = n,Hangup()\n\n"
} >> $name-juke.conf

album() {
printf "exten = _%02dXX,1,Set(aa=$rootfolder/$folder)\n" $a
printf "same = n,GoToIf(\$[\"\${EXTEN:-2}\" = \"00\"]\?full)\nsame = n,Goto($name-juke-one,\${EXTEN:-2},1)\nsame = n(full),Set(tt=$tracks)\nsame = n,Goto($name-juke-full,s,1)\n\n"
} >> $name-juke.conf

playback(){
printf "[$name-juke-one]\n"
[ "$leading" -ne 1 ] && printf "exten = _0X,1,Wait(.25)\nsame= n,Background(\${aa}/\${EXTEN:-1})\nsame= n,Goto($name-main,s,1)\n"
printf "exten = _XX,1,Wait(.25)\nsame= n,Background(\${aa}/\${EXTEN})\nsame = n,Goto($name-main,s,return)\nexten = 0,1,Goto($name-main,s,return)\n[$name-juke-full]\nexten = s,1,Set(t=1)\n"
[ "$leading" -eq 1 ] && printf "same = n(lead),GoToIf(\$[\${t} > 9]?hax)\nsame = n,Background(\${aa}/0\${t})\nsame = n,Goto(s,next)\n"
printf "same = n(hax),Background(\${aa}/\${t})\nsame = n(next),Set(t=\$[\${t} + 1])\nsame = n(skip),GoToIf(\$[\${t} < \${tt}]?$pb)\nsame= n,Background($rootfolder/$goodbye)\nsame = n,Hangup()\n"
printf "exten = 1,1,GoToIf(\$[\"\${t}\" = \"1\"]?wait)\nsame = n,Set(t=\$[\${t} - 1])\nsame = n(wait),Wait(.25)\nsame = n,Goto(s,$pb)\n"
printf "exten = 2,1,Wait(.25)\nsame = n,Goto(s,$pb)\nexten = 3,1,Wait(.25)\nsame = n,Goto(s,next)\nexten = 0,1,Goto($name-main,s,return)"
} >> $name-juke.conf

printf "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n|j|a|w| - jukebox for asterisk wizard\n  version .73    -    Jay Moore/NQ4T\n   https://gitlab.com/dewdude/jaw\n+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n\n\n"
printf "Pick a name/identifier for this jukebox: "
read name
printf "\nEnter the root folder name of your jukebox: "
read rootfolder
printf "\nEnter the filename of your menu/intro: "
read introfile
printf "\nHow many times do you want it to play? "
read loop
printf "\nHow many seconds between intro play? "
read delay
printf "\nEnter the filename of your exit file: "
read goodbye
printf "\nEnter a channel volume (I recommend -3): "
read vol
printf "\nEnter 1 if tracks < 10 have leading 0: (THIS MUST BE 1 OR 0!) "
read leading
[ "$leading" -eq 1 ] && pb=lead || pb=hax
printf "\nWriting Basic Configuration.\n\nAuto-Generating Albums\n\n"
setup;

# If your default directory isn't /var/lib/asterisk/sounds/en then modify.

dir=$(ls /var/lib/asterisk/sounds/en/$rootfolder -l | awk '/^d/ {print $9}')
a=0
for folder in $dir
do
tracks=$(ls /var/lib/asterisk/sounds/en/$rootfolder/$folder -l | grep "ulaw" | wc -l)
tracks=$((tracks + 1))
album;
a=$((a + 1))
done

printf "\n\nAlbum Generation Complete. Writing playback routines\n\n"

playback;

printf "\n\nDone.\n\n\nYour jukebox is located at $name-juke.conf.\n"