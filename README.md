# jaw - Jukebox for Asterisk Wizard

Builds a dialplan to make Asterisk function as a jukebox.

## About

An Asterisk Jukebox is a IVR that mimics the function of a CD jukebox. It
currently supports playing single tracks or full albums. Selections are made
using a 4 digit code; album number and track number. Basic playback controls
are available during album play to skip back, ahead, and repeat track.

jaw is a wizard for building the jukebox dialplan. After uploading your files
in the proper folder/filename convention, just answer the jaw's questions; it
will automagically spit out a jukebox dialplan.

## How To Use/Setup

Asterisk Jukebox depends heavily on your files being stored in a specific way; 
each album is it's own folder, all filenames are track numbers, and the jukebox
folder lives in Asterisk's sounds directory.

```
/var/lib/asterisk/sounds/en/jukeboxroot/album/[(0)1-99].ulaw
```
The .73 update brings some major changes to the wizard. You no longer need
to have leading zeros for tracks under 10; just specify in the prompt whether
you do. But everything for that jukebox must follow the same convention.

You no longer tell it how many albums you have, how many folders, or tracks.
The script now automatically does all of this; and as a result, albums are added
in directory list order. If you want a specific order, prefix your folder names. 

The automatic nature of the script does require your folder/files to already be
in the default asterisk directory; and that you can read it during execution. This
may require you to run the script as root. If your default directory isn't
/var/lib/asterisk/sounds/en then you must modify the script. 

Once the files are in place and you have read access, run the script and answer
the prompts:

"Pick a name/identifier for this jukebox:" - name used for dialplan context

"Enter the root folder name of your jukebox:" - folder in sounds that contain
all your albums

"Enter the filename of your menu/intro:" - name of file played when answered.
lives in juke root.

"How many times do you want it to play?" - how many times to play the intro
before hanging up

"How many seconds between intro play?" - how many seconds between intro
 playback

"Enter the filename of your exit file:" - name of file played before hangup.
lives in juke root.

"How many albums:" - the number of albums to enter

"Enter 1 if tracks < 10 have leading 0:" - Script looks for 1 or 0.

The script is now fully automatic from this point; gathering each folder in the
jukebox root, counting the tracks, and writing the config. 

Copy the conf to your asterisk directory and include it in your extensions.conf

```
[globals]
#include $name-juke.conf
```
Then it's just a matter of directing a call to it's context (usually s,$name-juke).

## Using The Jukebox

When you dial the extension you're greeted with your intro file. You then need
to enter a 4 digit code comprised of the album and track number:

```
1234
| |_________Track number
|___________Album number
```

Track number "00" is special and when entered, will play the entire album back
from the beginning. In album playback, the following controls affect playback:

1 - skips back a track
2 - repeats the current track
3 - skips ahead a track

At the end of the album, or at any time during playback; pressing 0 returns to
the main menu.

Single track playback returns to the main menu at the end of the track, or by
pressing 0.

# History

```
19-NOV-2021 - Intitial Commit
19-NOV-2021 - Additional Settings. More Confusing To Read
20-NOV-2021 - Finally fixed that major bug in playback.
		Replaced all echo with printf.
21-NOV-2021 - Added explanation of dialplan
22-NOV-2021 - Fixed "0 to menu" bug in album playback.
		Added volume input prompt.
01-MAY-2021 - Major Update: More automation, better dialplan

```

# Explanation of Generated Dialplan

The dialplan has undergone some extensive changes since last update. The
use of While has been eliminated as it caused some problems. The way some
things are done have been drastically changed (hopefully for the better). Some
of this was necessitated by the wizard; some of it was in spite of the wizard. 

```
[foobar-main]
exten = s,1,Answer()			; Hello, Bananaphone
same =n,Set(VOLUME(TX)=-3)		; my Candian friend told me everything was too loud
same = n(return),Set(m=1)		; counter for loop
same = n(loop),Background(foobar/foo)	; play background/intro
same = n,WaitExten(5)			; Wait 5 seconds
same = n,Set(m=$[${m} + 1])		; increment the counter
same = n,GoToIf($["${m}" < "5"]?loop)	; Protip: don't use while unless absolutely necessary
same = n,Background(foobar/bar)	; outro file
same = n,Hangup()			; "and your mother!" *SLAM!*
```

I don't think much has changed here other than replacing the while with a few
labels and gotoif. I had issues with whileloops nesting and causing problems.

```
exten = _00XX,1,Set(aa=bar/foobar)		; pattern match for album and write folder location
same = n,GoToIf($["${EXTEN:-2}" = "00"]?full)	; check for 00 in track number
same = n,Goto(foo-juke-one,${EXTEN:-2},1)	; go to single playback routine, use stripped ext
same = n(full),Set(tt=6)				; this is one more than actual album track number			
same = n,Goto(foo-juke-full,s,1)			; go to album playback routine

exten = _01XX,1,Set(aa=bar2/bar)		; Second verse, same as the first. A little bit louder a little bit worse
same = n,GoToIf($["${EXTEN:-2}" = "00"]?full)	; I'm Henry the eighth I am. Henry the eighth I am I am
same = n,Goto(foo2-juke-one,${EXTEN:-2},1)	; I got married to the window next door, she's been married seven times before
same = n(full),Set(tt=5)				; And ev'ry one was a Henry (Henry!). She wouldn't have a Willy or a Sam (no Sam)
same = n,Goto(foo-juke-full,s,1)			; I'm her eighth old man, I'm Henry. Henry the eighth I am.
```
We no longer write the last two of the extension to a variable, we just call it
directly from now on. We also now use that as the extension we dial in the
single playback routine. 

```
[foo-juke-one]					; single playback routine
;exten = _0X,1,Wait(.25)				; extension for tracks without leading 0
;same= n,Background(${aa}/${EXTEN:-1})	; pull only the last digit of called extension, play
;same= n,Goto(foo-main,s,1)			; go back to main menu	
exten = _XX,1,Wait(.25)				; tracks with leading 0 or > 10
same= n,Background(${aa}/${EXTEN})		; play file
same = n,Goto(foo-main,s,return)		; go home

exten = 0,1,Goto(foo-main,s,return)		; press 0 to go home
```
So the big change here is the fact we now use pattern matching. In cases where
you don't have leading zeros in the file name; then we match extensions starting
with 0 and just use the last digit. This extension is only written if needed. The 
second extension matches anything and just passes it. It's always written as it's
used for tracks over 10 and leading 0 filenames.

```
[foo-juke-full]					; full album context
exten = s,1,Set(t=1)				; reset track counter
;same = n(lead),GoToIf($[${t} > 9]?hax)		; leading 0 file name hack
;same = n,Background(${aa}/0${t})		; only written if needed
;same = n,Goto(s,next)				; make sure you pick the right option in wizard
same = n(hax),Background(${aa}/${t})		; play the thing
same = n(next),Set(t=$[${t} + 1])			; increment the track counter
same = n(skip),GoToIf($[${t} < ${tt}]?hax)		; Asterisk while sucks. We'll make our own.
;same = n(skip),GoToIf($[${t} < ${tt}]?lead)	; This line is used instead if you have leading 0s
same= n,Background(bar/bar)			; play the outro
same = n,Hangup()				; GTFO

exten = 1,1,GoToIf($["${t}" = "1"]?wait)		; We can't play track 0.
same = n,Set(t=$[${t} - 1])			; decrement track counter
same = n(wait),Wait(.25)				; wait for dtmf
same = n,Goto(s,hax)				; go play the thing
;same = n,Goto(s,lead)				; if leading 0s is true

exten = 2,1,Wait(.25)				; shut up DTMF
same = n,Goto(s,hax)				; go play the thing
;same = n,Goto(s,lead)				; if leading 0s is true

exten = 3,1,Wait(.25)				; DTMF hax
same = n,Goto(s,next)				; go to next track
exten = 0,1,Goto(foo-main,s,return)		; go home dad you're drunk
```

The another elimination of the while loops makes for a big change. The hack for
leading 0's is now optional; and a few more labels from eliminting the loop and
being able to skip writing the hack bring it together. The skip extension no longer
actually does math, but just directs to the dialplan line that does. 

# License

    jaw - jukebox for asterisk wizard
    Copyright (C) 2021 Jay Moore - nq4tango@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


